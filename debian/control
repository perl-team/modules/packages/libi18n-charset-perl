Source: libi18n-charset-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Niko Tyni <ntyni@iki.fi>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-install-perl
Build-Depends-Indep: libio-capture-perl <!nocheck>,
                     libio-string-perl <!nocheck>,
                     libtest-pod-coverage-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     libunicode-map-perl <!nocheck>,
                     libunicode-maputf8-perl <!nocheck>,
                     perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libi18n-charset-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libi18n-charset-perl.git
Homepage: https://metacpan.org/release/I18N-Charset
Rules-Requires-Root: no

Package: libi18n-charset-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libio-string-perl
Suggests: libjcode-pm-perl,
          libunicode-map-perl,
          libunicode-map8-perl,
          libunicode-maputf8-perl
Description: Perl module for mapping character set names to IANA names
 I18N::Charset maps Character Set names to the names officially
 registered with IANA.  For example, 'Shift_JIS' is the official name
 of 'x-sjis'.
 .
 It also maps character set names to Unicode::Map, Unicode::Map8, and
 Unicode::MapUTF8 conversion scheme names (if those modules are
 installed).  For example, the Unicode::Map8 scheme name for
 'windows-1251' is 'cp1251'.
